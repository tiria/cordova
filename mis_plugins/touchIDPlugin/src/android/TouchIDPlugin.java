package com.alejandrotiria.plugin;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.Executor;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;

public class TouchIDPlugin extends CordovaPlugin {

    public static final String TAG = "TouchIDPlugin";
    CallbackContext context;

    private Executor executor;
    private BiometricPrompt biometricPrompt;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

        if (!action.equals("verifyFingerprint")) {
            Log.e(TAG, "execute: " + action + "\" is not a recognized action.");
            callbackContext.error("\"" + action + "\" is not a recognized action.");
            return false;
        }

        // Create the BiometricManager and check if the user can use the fingerprint sensor or not
        BiometricManager biometricManager = BiometricManager.from(cordova.getContext());

        switch (biometricManager.canAuthenticate(BIOMETRIC_STRONG | DEVICE_CREDENTIAL)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d(TAG, "App can authenticate using biometrics.");
                Toast.makeText(cordova.getContext(), "App can authenticate using biometrics.", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e(TAG, "No biometric features available on this device.");
                Toast.makeText(cordova.getContext(), "No biometric features available on this device.", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e(TAG, "Biometric features are currently unavailable.");
                Toast.makeText(cordova.getContext(), "Biometric features are currently unavailable.", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.e(TAG, "Your device don't have any fingerprint saved, please check your security settings");
                Toast.makeText(cordova.getContext(), "Your device don't have any fingerprint saved, please check your security settings", Toast.LENGTH_SHORT).show();
                break;
        }

        // Biometric dialog box
        Executor executor = ContextCompat.getMainExecutor(cordova.getActivity());
        // Create the biometric promp callback
        BiometricPrompt biometricPrompt = new BiometricPrompt((FragmentActivity) cordova.getActivity(), executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(cordova.getContext(), "Login Success!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }
        });

        // Biometric Dialog
        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Login")
                .setDescription("User your fingerprint to login to your app")
                .setNegativeButtonText("Cancel")
                .build();

        // Call the dialog when the user press the login button
        biometricPrompt.authenticate(promptInfo);

        context = callbackContext;
        return true;

    }


}