package com.alejandrotiria.plugin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ContactsPlugin extends CordovaPlugin {

    public static final String TAG = "ContactsPlugin";
    public static final String READ_CONTACTS = Manifest.permission.READ_CONTACTS;
    public static final int SEARCH_REQ_CODE = 0;

    private ArrayList contactsArray;
    private final String DISPLAY_NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;
    private final String FILTER = DISPLAY_NAME + " NOT LIKE '%@%'";
    private final String ORDER = String.format("%1$s COLLATE NOCASE", DISPLAY_NAME);
    CallbackContext context;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

        // Verify that the user sent a 'show' action
        if (!action.equals("getContacts")) {
            Log.e(TAG, "execute: " + action + "\" is not a recognized action.");
            callbackContext.error("\"" + action + "\" is not a recognized action.");
            return false;
        }

        context = callbackContext;

        if (cordova.hasPermission(READ_CONTACTS)) {

            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    contactsArray = getAllContacts();
                    Log.d(TAG, "execute: contactos" + contactsArray);
                    JSONArray jsArray = new JSONArray(contactsArray);
                    callbackContext.success(jsArray);
                }
            });

            return true;
        } else {
            this.getReadContactsPermission(SEARCH_REQ_CODE);
            return true;
        }

    }

    protected void getReadContactsPermission(int requestCode) {
        cordova.requestPermission(this, requestCode, READ_CONTACTS);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException {
        PluginResult result;

        if (context != null) {
            for (int r : grantResults) {
                if (r == PackageManager.PERMISSION_DENIED) {
                    LOG.d(TAG, "Permission Denied!");
                    result = new PluginResult(PluginResult.Status.ILLEGAL_ACCESS_EXCEPTION);
                    context.sendPluginResult(result);
                    return;
                }

            }

            contactsArray = getAllContacts();
            Log.d(TAG, "execute: contactos" + contactsArray);
            JSONArray jsArray = new JSONArray(contactsArray);
            context.success(jsArray);
        }
    }

    @SuppressLint("InlinedApi")
    private final String[] PROJECTION = {
            ContactsContract.Contacts._ID,
            DISPLAY_NAME,
            ContactsContract.Contacts.HAS_PHONE_NUMBER
    };

    private ArrayList getAllContacts() {
        try {
            ArrayList contacts = new ArrayList<>();

            ContentResolver cr = cordova.getActivity().getContentResolver();
            Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, FILTER, null, ORDER);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    // get the contact's information
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                    Integer hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    // get the user's email address
                    String email = null;
                    Cursor ce = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (ce != null && ce.moveToFirst()) {
                        email = ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        ce.close();
                    }

                    // get the user's phone number
                    String phone = null;
                    if (hasPhone > 0) {
                        Cursor cp = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        if (cp != null && cp.moveToFirst()) {
                            phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            cp.close();
                        }
                    }

                    // if the user user has an email or phone then add it to contacts
                    if ((!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
                            && !email.equalsIgnoreCase(name)) || (!TextUtils.isEmpty(phone))) {
                        JSONObject contact = new JSONObject();
                        contact.put("name", name);
                        contact.put("email", email);
                        contact.put("phoneNumber", phone);
                        Log.e(TAG, "getAllContacts: " + contact);
                        contacts.add(contact);
                    }

                } while (cursor.moveToNext());

                // clean up cursor
                cursor.close();
            }

            return contacts;
        } catch (Exception ex) {
            return null;
        }
    }


}