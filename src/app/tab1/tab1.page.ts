import { Component } from '@angular/core';
import { Coordinates } from '@ionic-native/geolocation/ngx';

import { GeolocationService } from '../services/geolocation.service';
import { WeatherService } from '../interfaces/weather';

@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
	public iconurl: string;
	public weather: WeatherService;
	public date: Date;
	public hours: string;
	public coords: Coordinates;

	constructor(private _geolocationService: GeolocationService) {}

	async ionViewWillEnter() {
		if (!this.coords) {
			this.date = new Date();
			this.hours = `${this.date.getHours()}:${this.date.getMinutes()}`;

			this.coords = await this._geolocationService.getCurrentPosition();

			this._geolocationService.getWeather(this.coords).subscribe(weather => {
				this.weather = weather;
				this.iconurl = `https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`;
				this.weather.main.temp = Math.round(this.weather.main.temp);
				this.weather.main.temp_max = Math.round(this.weather.main.temp_max);
				this.weather.main.temp_min = Math.round(this.weather.main.temp_min);
			});
		}
	}
}
