import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { HeaderSingleComponent } from './header-single/header-single.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
	imports: [IonicModule],
	declarations: [HeaderComponent, HeaderSingleComponent],
	exports: [HeaderComponent, HeaderSingleComponent],
})
export class ComponentsModule {}
