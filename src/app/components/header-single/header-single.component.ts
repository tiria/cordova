import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-header-single',
	templateUrl: './header-single.component.html',
	styleUrls: ['./header-single.component.scss'],
})
export class HeaderSingleComponent implements OnInit {
	@Input() title: string = 'Volver';

	constructor() {}

	ngOnInit() {}
}
