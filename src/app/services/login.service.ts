import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AlertController } from '@ionic/angular';

import { DataForm } from '../interfaces/data-form';
import { Login } from '../interfaces/login';

@Injectable({
	providedIn: 'root',
})
export class LoginService {
	constructor(
		private http: HttpClient,
		private alertCtrl: AlertController,
		private router: Router
	) {}

	login(formData: Login) {
		return this.http.get<DataForm>('assets/data/dataForm.json').pipe(
			map(async resp => {
				if (
					parseInt(formData.documento) !== parseInt(resp.login.documento) ||
					parseInt(formData.clave) !== parseInt(resp.login.clave)
				) {
					const alert = await this.alertCtrl.create({
						header: 'Datos incorrectos',
						message: 'Verifique los datos ingresados',
						buttons: ['Aceptar'],
					});

					await alert.present();
				} else if (formData.recordar) {
					localStorage.setItem('documento', formData.documento);
					this.router.navigateByUrl('/tabs/tab1');
				} else {
					localStorage.removeItem('documento');
					this.router.navigateByUrl('/tabs/tab1');
				}
			})
		);
	}
}
