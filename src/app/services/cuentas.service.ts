import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { Cuenta } from '../interfaces/cuenta';
import { DataForm } from '../interfaces/data-form';

@Injectable({
	providedIn: 'root',
})
export class CuentasService {
	public cuenta: Cuenta;

	constructor(private http: HttpClient) {}

	getAccounts() {
		return this.http.get<DataForm>('assets/data/dataForm.json').pipe(
			map(resp => {
				return resp.cuentas;
			})
		);
	}
}
