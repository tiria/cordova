import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { WeatherService } from '../interfaces/weather';
import { Coordinates } from '@ionic-native/geolocation/ngx';

const API_WEATHER = environment.API_WEATHER;
const API_KEY_WEATHER = environment.API_KEY_WEATHER;

@Injectable({
	providedIn: 'root',
})
export class GeolocationService {
	constructor(private http: HttpClient) {}

	getCurrentPosition(): Promise<Coordinates> {
		return new Promise((resolve, reject) => {
			var options = {
				enableHighAccuracy: true,
				timeout: 5000,
				maximumAge: 0,
			};

			function success(pos) {
				resolve(pos.coords);
			}

			function error(err) {
				console.warn('ERROR(' + err.code + '): ' + err.message);
				reject(err);
			}

			navigator.geolocation.getCurrentPosition(success, error, options);
		});
	}

	getWeather(coords: Coordinates) {
		return this.http.get<WeatherService>(
			`${API_WEATHER}?lat=${coords.latitude}&lon=${coords.longitude}&appid=${API_KEY_WEATHER}&lang=es&units=metric`
		);
	}
}
