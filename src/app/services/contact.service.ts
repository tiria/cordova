import { Injectable } from '@angular/core';
import { Contacto } from '../interfaces/contacto';

declare const window: any;

@Injectable({
	providedIn: 'root',
})
export class ContactService {
	public contacto: Contacto;

	constructor() {}

	getContacts(): Promise<Contacto[]> {
		return new Promise((resolve, reject) => {
			window.plugins.contactsPlugin.getContacts(
				(data: Contacto[]) => {
					resolve(data);
				},
				err => {
					console.error('Ocurrio un error', err);
					reject(err);
				}
			);
		});
	}
}
