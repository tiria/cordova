import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'tabs',
		loadChildren: () =>
			import('./tabs/tabs.module').then(m => m.TabsPageModule),
	},
	{
		path: 'login',
		loadChildren: () =>
			import('./login/login.module').then(m => m.LoginPageModule),
	},
	{
		path: 'contacto',
		loadChildren: () =>
			import('./pages/contacto/contacto.module').then(
				m => m.ContactoPageModule
			),
	},
	{
		path: 'cuenta',
		loadChildren: () =>
			import('./pages/cuenta/cuenta.module').then(m => m.CuentaPageModule),
	},
	{
		path: 'generar-qr',
		loadChildren: () =>
			import('./pages/generar-qr/generar-qr.module').then(
				m => m.GenerarQRPageModule
			),
	},
	{
		path: '',
		redirectTo: '/login',
		pathMatch: 'full',
	},
	{
		path: '**',
		redirectTo: '/login',
	},
];
@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
