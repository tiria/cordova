import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { CuentasService } from 'src/app/services/cuentas.service';
import { Cuenta } from 'src/app/interfaces/cuenta';

@Component({
	selector: 'app-generar-qr',
	templateUrl: './generar-qr.page.html',
	styleUrls: ['./generar-qr.page.scss'],
})
export class GenerarQRPage implements OnInit {
	public isActivatedButton: boolean = false;
	public producto: number = 0;
	public showErr: boolean = false;
	public textLabelErr: string = 'Ingrese un número de 11 dígitos';
	public cuentas: Cuenta[] = [];
	public totalSlides: number = 0;
	public activeIndex: number = 0;
	public dataGenerateQR = {
		productDestination: '',
		productType: 'ahorros',
		productOrigin: '',
		value: '',
	};
	public slidesOptions = {
		initialSlide: 0,
		allowSlideNext: false,
		allowSlidePrev: false,
	};

	@ViewChild('accountSlider') slides: IonSlides;

	constructor(
		private router: Router,
		private barcodeScanner: BarcodeScanner,
		private _cuentasService: CuentasService
	) {}

	ngOnInit() {
		this._cuentasService.getAccounts().subscribe(cuentas => {
			this.cuentas = cuentas;
		});
	}

	async ionViewDidEnter() {
		this.activeIndex = await this.slides.getActiveIndex();
		this.totalSlides = await this.slides.length();
	}

	bloquearSlides() {
		this.slides.lockSwipeToNext(true);
		this.slides.lockSwipeToPrev(true);
	}

	desbloquearSlides() {
		this.slides.lockSwipeToNext(false);
		this.slides.lockSwipeToPrev(false);
	}

	async swipeNext() {
		this.desbloquearSlides();
		this.slides.slideNext();
		this.bloquearSlides();
		this.isActivatedButton = false;
		this.activeIndex = await this.slides.getActiveIndex();
	}

	validarInput(value: any, name: string) {
		switch (name) {
			case 'valor':
				if (value >= 10000) {
					this.isActivatedButton = true;
					this.showErr = false;
				} else {
					this.isActivatedButton = false;
				}
				break;
			case 'producto':
				if (value.toString().length === 11) {
					this.isActivatedButton = true;
					this.showErr = false;
				} else {
					this.isActivatedButton = false;
				}
				break;
		}
	}

	inputBlur(value: any, name: string) {
		switch (name) {
			case 'valor':
				if (value >= 10000) {
					this.showErr = false;
				} else {
					this.textLabelErr = 'El valor mínimo es $10.000';
					this.showErr = true;
				}
				break;
			case 'producto':
				if (value.toString().length === 11) {
					this.showErr = false;
				} else {
					this.showErr = true;
				}
				break;
		}
	}

	selectProduct(event: any, name: string) {
		switch (name) {
			case 'cuenta':
				if (event.detail.value) {
					this.dataGenerateQR.productOrigin = event.detail.value;
					this.isActivatedButton = true;
				} else {
					this.isActivatedButton = false;
				}
				break;
			case 'tipo':
				this.dataGenerateQR.productType = event.detail.value;
				break;
		}
	}

	generateBarCode() {
		this.barcodeScanner
			.encode(this.barcodeScanner.Encode.TEXT_TYPE, this.dataGenerateQR)
			.then(
				res => {
					alert(res);
				},
				error => {
					alert(error);
					console.error(error);
				}
			);

		this.router.navigateByUrl('/tabs/tab2');
	}
}
