import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerarQRPageRoutingModule } from './generar-qr-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

import { GenerarQRPage } from './generar-qr.page';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		GenerarQRPageRoutingModule,
		ComponentsModule,
	],
	declarations: [GenerarQRPage],
})
export class GenerarQRPageModule {}
