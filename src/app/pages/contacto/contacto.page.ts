import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';

@Component({
	selector: 'app-contacto',
	templateUrl: './contacto.page.html',
	styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {
	constructor(public _contactService: ContactService) {}

	ngOnInit() {}
}
