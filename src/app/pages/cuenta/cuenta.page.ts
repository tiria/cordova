import { Component, OnInit } from '@angular/core';
import { CuentasService } from 'src/app/services/cuentas.service';

@Component({
	selector: 'app-cuenta',
	templateUrl: './cuenta.page.html',
	styleUrls: ['./cuenta.page.scss'],
})
export class CuentaPage implements OnInit {
	constructor(public _cuentasService: CuentasService) {}

	ngOnInit() {}
}
