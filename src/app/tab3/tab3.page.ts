import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { ContactService } from '../services/contact.service';
import { Contacto } from '../interfaces/contacto';

@Component({
	selector: 'app-tab3',
	templateUrl: 'tab3.page.html',
	styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
	public contactos: Contacto[] = [];
	public showSkeleton: boolean = true;

	constructor(
		private router: Router,
		private platform: Platform,
		public _contactService: ContactService
	) {}

	ionViewWillEnter() {
		if (!this.platform.is('cordova') || !this.platform.is('android')) {
			console.warn('No es un proyecto cordova!!');
			this.showSkeleton = false;
			return;
		}

		if (this.contactos.length === 0) {
			this.showSkeleton = true;
			this._contactService
				.getContacts()
				.then(contactos => {
					this.showSkeleton = false;
					this.contactos = contactos;
				})
				.catch(err => {
					console.error(err);
					this.showSkeleton = false;
				});
		}
	}

	verContacto(contacto: Contacto) {
		if (!contacto) {
			return;
		}

		this._contactService.contacto = contacto;

		this.router.navigateByUrl('/contacto');
	}
}
