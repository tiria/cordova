import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from '../services/login.service';

declare const window: any;

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
	public loginForm: FormGroup;
	public documento: string;
	public showTouchID: boolean = false;

	constructor(
		private fb: FormBuilder,
		private _loginService: LoginService,
		private platform: Platform
	) {}

	async ngOnInit() {
		this.documento = localStorage.getItem('documento') || '';

		this.loginForm = this.fb.group({
			documento: [
				this.documento,
				[Validators.required, Validators.minLength(7)],
			],
			clave: [
				'',
				[Validators.required, Validators.minLength(4), Validators.maxLength(4)],
			],
			recordar: [this.documento ? true : false],
		});

		await this.platform.ready();

		if (this.platform.is('cordova') && this.platform.is('android')) {
			window.plugins.touchIDPlugin.verifyFingerprint(
				data => {
					console.log(data);
				},
				err => {
					console.error('Ocurrio un error', err);
				}
			);
		} else {
			console.warn('No es un proyecto cordova!!');
			return;
		}
	}

	login() {
		if (this.loginForm.invalid) {
			return;
		}

		this._loginService.login(this.loginForm.value).subscribe();
	}

	loginTouchID() {
		console.log('click...');
	}
}
