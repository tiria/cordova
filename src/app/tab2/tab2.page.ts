import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
	BarcodeScanner,
	BarcodeScannerOptions,
} from '@ionic-native/barcode-scanner/ngx';
import { ToastController } from '@ionic/angular';

import { CuentasService } from '../services/cuentas.service';
import { Cuenta } from '../interfaces/cuenta';

@Component({
	selector: 'app-tab2',
	templateUrl: 'tab2.page.html',
	styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
	public cuentas: Cuenta[] = [];
	public barcodeScannerOptions: BarcodeScannerOptions;

	constructor(
		private router: Router,
		private toastCtrl: ToastController,
		private barcodeScanner: BarcodeScanner,
		private _cuentasService: CuentasService
	) {}

	ngOnInit() {
		this.barcodeScannerOptions = {
			showTorchButton: true,
			showFlipCameraButton: true,
		};

		this._cuentasService.getAccounts().subscribe(cuentas => {
			this.cuentas = cuentas;
		});
	}

	scanBRcode() {
		this.barcodeScanner
			.scan()
			.then(barcodeData => {
				const cuentaScan = JSON.parse(barcodeData.text);
				let existeCuenta: boolean = false;

				this.cuentas.filter((cuenta, index) => {
					if (cuenta.number === cuentaScan.productDestination.toString()) {
						this.cuentas[index].movimientos.unshift({
							date: new Date(),
							type: 'Transferencia con código QR',
							valor: cuentaScan.value,
							ingreso: true,
						});
						this.cuentas[index].saldo = (
							Number(this.cuentas[index].saldo) + Number(cuentaScan.value)
						).toString();

						existeCuenta = true;

						this.presentToast('Transferencia realizada!!');
					}
				});

				if (!existeCuenta) {
					this.presentToast(
						`La cuenta ${cuentaScan.productDestination} no existe`
					);
				}
			})
			.catch(err => {
				console.error('Error', err);
			});
	}

	generateBarCode() {
		this.router.navigateByUrl('/generar-qr');
	}

	verCuenta(cuenta: Cuenta) {
		this._cuentasService.cuenta = cuenta;
		this.router.navigateByUrl('/cuenta');
	}

	async presentToast(message: string) {
		const toast = await this.toastCtrl.create({
			message,
			duration: 3000,
		});
		toast.present();
	}
}
