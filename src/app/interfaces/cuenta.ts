export interface Cuenta {
	type: string;
	number: string;
	saldo: string;
	movimientos?: Movimiento[];
}

interface Movimiento {
	date: Date;
	type: string;
	valor: string;
	ingreso: boolean;
}
