import { Login } from './login';
import { Cuenta } from './cuenta';

export interface DataForm {
	login: Login;
	cuentas: Cuenta[];
}
