export interface Contacto {
	name: string;
	email: string;
	phoneNumber: string;
}
